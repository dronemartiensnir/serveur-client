#ifndef BDD_h
#define BDD_h

#include <iostream>
#include "mariadb/mysql.h"
#include <string>

using namespace  std;
using std::string;

class BDD {

private:
	MYSQL* mysql = mysql_init(NULL);
	MYSQL_RES* result = NULL;
	MYSQL_ROW row = NULL;
	string query;

public:
	BDD();
	bool IsConnect();
	void SetQuery();
	void SetQuery(string col_, string table_);
	string GetQuery();
	void display();
	string GetTable();
};

#endif
