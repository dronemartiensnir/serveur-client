#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "boost/asio.hpp"
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "con_handler.h"
#include "Server.h"
#include "BDD.h"

using namespace  std;
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

	typedef boost::shared_ptr<con_handler> pointer;

//Envoie de la BDD en socket

	con_handler::con_handler(boost::asio::io_service& io_service): sock(io_service)
	{
	}

	pointer con_handler::create(boost::asio::io_service& io_service)
	{
		return pointer(new con_handler(io_service));
	}

	tcp::socket& con_handler::socket()
	{
		return sock;
	}

	//start

	void con_handler::start()
	{
		sock.async_read_some(
			boost::asio::buffer(data, max_length),
			boost::bind(&con_handler::handle_read,
				shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));


		sock.async_write_some(
			boost::asio::buffer(msg_send, max_length),
			boost::bind(&con_handler::handle_write,
				shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred));
	}

	//Read
	void con_handler::handle_read(const boost::system::error_code& err, size_t bytes_transferred)
	{
		if (!err) {
			if ((data[0] == 'D')&&(data[1] == 'M')&&(data[2] == '_')) {
				cout << "Msg recu :"<<data << endl;
			}
			else {
				cout << "Mauvais message de recep!" << endl;
				sock.close();
			}
		}
		else {
			std::cerr << "err (recv): " << err.message() << std::endl;
			sock.close();
		}
	}

	//Write
	void con_handler::handle_write(const boost::system::error_code& err, size_t bytes_transferred)
	{
		if (!err) {
			cout << "Serveur send : " << msg_send<<endl;
		}
		else {
			cout << "err (recv): " << err.message() << endl;
			sock.close();
		}
	}

	void con_handler::set_MsgSend(string msg_)
	{
		msg_send = msg_;
	}