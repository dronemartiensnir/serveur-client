#include "mainwindow.h"

#include <QApplication>

#include "con_handler.h"
#include "server.h"
#include "bdd.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "D:\Programmation\boost_1_72_0\boost\asio.hpp"
#include <D:\Programmation\boost_1_72_0\boost\bind.hpp>
#include <D:\Programmation\boost_1_72_0\boost\enable_shared_from_this.hpp>

using namespace  std;
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();

    try
    {
        boost::asio::io_service io_service;
        Server server(io_service);
        io_service.run();
    }
    catch (std::exception & e)
    {
        std::cerr << e.what() << endl;
    }
    return 0;
}


