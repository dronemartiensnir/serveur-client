#ifndef Server_h
#define Server_h

#include <stdio.h>
#include <con_handler.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "D:\Programmation\boost_1_72_0\boost\asio.hpp"
#include <D:\Programmation\boost_1_72_0\boost\bind.hpp>
#include <D:\Programmation\boost_1_72_0\boost\enable_shared_from_this.hpp>

using namespace  std;
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

class Server {

private:
    tcp::acceptor acceptor_;
    void start_accept();

public:
    Server(boost::asio::io_service& io_service);
    void handle_accept(con_handler::pointer connection, const boost::system::error_code& err);
};

#endif
