#include "server.h"
#include "bdd.h"
#include "con_handler.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "D:\Programmation\boost_1_72_0\boost\asio.hpp"
#include <D:\Programmation\boost_1_72_0\boost\bind.hpp>
#include <D:\Programmation\boost_1_72_0\boost\enable_shared_from_this.hpp>
using namespace std;
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

    Server::Server(boost::asio::io_service& io_service) : acceptor_(io_service, tcp::endpoint(tcp::v4(), 1234))
    {
        start_accept();
    }

    void Server::start_accept()
    {
        //Create bdd
        BDD DataBase = new BDD();
        DataBase.SetQuery();


        // creates a socket
        con_handler::pointer connection =
        con_handler::create(acceptor_.get_io_service());
        connection.Set_MsgSend(DataBase.GetTable());

        // initiates an asynchronous accept operation
        // to wait for a new connection.
        acceptor_.async_accept(connection->socket(),
            boost::bind(&Server::handle_accept, this, connection,
                boost::asio::placeholders::error));
    }

    void Server::handle_accept(con_handler::pointer connection, const boost::system::error_code& err)
    {
        if (!err) {
            connection->start();
        }
    start_accept();
    }
