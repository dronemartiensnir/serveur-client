#ifndef con_handler_h
#define con_handler_h

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "D:\Programmation\boost_1_72_0\boost\asio.hpp"
#include <D:\Programmation\boost_1_72_0\boost\bind.hpp>
#include <D:\Programmation\boost_1_72_0\boost\enable_shared_from_this.hpp>

using namespace  std;
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

//Envoie de la BDD en socket

class con_handler : public boost::enable_shared_from_this<con_handler> {

private:
    tcp::socket sock;
    const string proto ="DM_";

    string msg_send = proto + "Il n'y a pas de mission";

    enum { max_length = 1024 };
    char data[max_length];

public:
    typedef boost::shared_ptr<con_handler> pointer;

    con_handler(boost::asio::io_service& io_service);

    static pointer create(boost::asio::io_service& io_service);

    tcp::socket& socket();

    //start
    void start();

    //Read
    void handle_read(const boost::system::error_code& err, size_t bytes_transferred);

    //Write
    void handle_write(const boost::system::error_code& err, size_t bytes_transferred);

    void set_MsgSend(string msg_);
};

#endif
