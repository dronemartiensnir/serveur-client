#include "con_handler.h"
#include "Server.h"
#include "BDD.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "boost/asio.hpp"
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>


using namespace  std;
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
	try
	{
		boost::asio::io_service io_service;
		Server server(io_service);
		io_service.run();
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << endl;
	}
	return 0;
}